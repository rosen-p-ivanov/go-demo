package main

import "fmt"

func add(x int, y int) int {
	return x + y
}

func swap(x, y string) (string, string) {
	return y, x
}

func split(sum int) (x, y int) { // x and y are named results
	x = sum * 4 / 9
	y = sum - x
	return
}

func intSeq() func() int {  //returns function returning int
	i := 0
	return func() int {  //returns anonymouse function
			i++
			return i
	}
}

func main() {
	fmt.Println(add(42, 13)) //cannot be more simple

	a, b := swap("hello", "world") //multiple returned values
	fmt.Println(a, b)
	fmt.Println(split(17))

	nextInt := intSeq();
	fmt.Println("closure example");		
	fmt.Println(nextInt())
  fmt.Println(nextInt())
	fmt.Println(nextInt())
	newInts := intSeq()
  fmt.Println(newInts())
}

package main

import "fmt"

const(
	constString string = "just as tring"
)

const constString2 = "just as tring"


func primitiveTypes(){
	fmt.Println("Hello, Go!")
	fmt.Println(constString)	

	var (
		i int
	  f float64
	  b bool
	)
	
	var s string	
	fmt.Printf("%v %v %v %q\n", i, f, b, s)

	shortDeclaration := 123.123
	shortDeclaration = 200;
	fmt.Printf("%v", shortDeclaration)
	fmt.Printf("\n")
}

func arrayAndMapExamples(){
	var a [2]string 
  a[0] = "Hello"
	a[1] = "World"  
  fmt.Println(a[0], a[1])
	fmt.Println(a)
	
		var slice []string
		slice = a[:]
		fmt.Println(slice)
		
		cities := []string{"Sofia"} //declaration and intialization
	  cities = append(cities, "San Diego", "Mountain View")
		fmt.Printf("%q\n", cities)		

		pow := make([]int, 10) // initialize empty slice
	  for i:= range pow {			
		  pow[i] = 1 << uint(i)
		  if pow[i] >= 16 {
			  break
		  }
	  }
		fmt.Println(pow)
		
		celebs := map[string]int{
			"Nicolas Cage":       50,
			"Selena Gomez":       21,
			"Jude Law":           41,
			"Scarlett Johansson": 29, // wow "," is mandatory if there is no "}" on the same line
		}
	
		fmt.Printf("%v\n", celebs["Nicolas Cage"])	
}

func main() {
		
	//primitiveTypes()
	arrayAndMapExamples()
}

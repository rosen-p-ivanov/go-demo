package main

import "fmt"

type rect struct {
	width, height int
}

func (r *rect) area() int {
	return r.width * r.height
}

func (r rect) perim() int {
	return 2*r.width + 2*r.height
}


type User struct {
	FirstName, LastName string
	isGreeted bool
}

func (u *User) Greeting() string {
	u.isGreeted = true
	return fmt.Sprintf("Dear %s %s", u.FirstName, u.LastName)
}

func main() {
	r := rect{width: 10, height: 5}

	fmt.Println("area: ", r.area())
  fmt.Println("perim:", r.perim())
	
	rp := &r
  fmt.Println("area: ", rp.area())
	fmt.Println("perim:", rp.perim()) 
	

	u := &User{"Go", "Pointer", false} //passing pointer
	fmt.Println(u.Greeting()) //no new values are passed
	fmt.Printf("Is grreted %v\n", u.isGreeted)
}

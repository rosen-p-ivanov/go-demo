package main
import (
	"fmt"
)

func fibonacci(n int, c chan int) {
	x, y := 0, 1
	for i := 0; i < n; i++ {
			c <- x
			x, y = y, x+y
	}
	close(c) //explicit close of the channel: never do it in the calling routne -> panic
}

func main() {
	c := make(chan int, 15)
	go fibonacci(cap(c), c)  //we are exploiting buffered capacity: cap(c)
	for i := range c {
			fmt.Println(i)
	}
}

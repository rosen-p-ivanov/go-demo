package main

import (
	"fmt"
)
type Stereotype int

const (
    TypicalNoob Stereotype = iota // 0
    TypicalHipster                // 1
    TypicalUnixWizard             // 2
    TypicalStartupFounder         // 3
)
func CountAllTheThings(i int) string {
	return fmt.Sprintf("there are %d things", i)
}

func main() {
n := TypicalHipster
fmt.Println(CountAllTheThings(n))
}

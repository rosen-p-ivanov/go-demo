package main

import "fmt"

func main() {
	i := 42.2701

	p := &i         // point to i
	fmt.Println(p)  //actual memmory address
	fmt.Println(*p) // read i through the pointer
	fmt.Println(&i) // i value of the address	fmt.Println(&p) // adress of the  pointer
	
	
	*p = 21         // set i through the pointer
	fmt.Println(i)  // see the new value of i	

	var a *int
	fmt.Println(a == nil, a != nil)
	
	k:=&p
	fmt.Println(*k)
	fmt.Println(**k)
}

package main

import (
    "fmt"
    "runtime"
		"sync"
		"time"
)

func main() {
    runtime.GOMAXPROCS(2)
    fmt.Println(runtime.NumGoroutine())
    var wg sync.WaitGroup
    wg.Add(2)

    fmt.Println("Starting Go Routines")
    go func() {
        defer wg.Done() //defer will be executed when all surroundig functions return
				time.Sleep(1 * time.Microsecond)
        for char := 'a'; char < 'a'+26; char++ {
            fmt.Printf("%c ", char)
        }
    }()

    go func() {
        defer wg.Done()				
        for number := 1; number < 100; number++ {
            fmt.Printf("%d ", number)
        }
		}()
		
		fmt.Println(runtime.NumGoroutine())

    fmt.Println("Waiting To Finish")
    wg.Wait()

    fmt.Println("\nTerminating Program")
}
